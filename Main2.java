// Interface ToEur
public interface ToEur {
    public void toEuros();
}

// Interface ToUSD
public interface ToUSD {
    public void toUSD();
}

// Interface ToRu
public interface ToRu {
    public void toRu();
}

// Interfce ToMk
public interface ToMk {
    public void toMk();
}

// HrvatskaValuta.java
public class HrvatskaValuta implements ToEur, ToUSD, ToMk, ToRu {
    float hrkVrijednost;
    HrvatskaValuta(float hrkVrijednost){
        this.hrkVrijednost=hrkVrijednost;
    }

    public float getHrk() {
        return hrkVrijednost;
    }

    public void setHrk(float hrkVrijednost) {
        this.hrkVrijednost = hrkVrijednost;
    }

    public void toEuros() {
        // do something
        double euros = getHrk() * 0.13;
        System.out.println("\nEUR: " + euros);
    }

    public void toUSD() {
        // do something
        double dolla = getHrk() * 0.14;
        System.out.println("\nUSD: " + dolla);
    }

    public void toMk() {
        // do something
        double mk = getHrk() * 0.25;
        System.out.println("\nDeutshce Mark: " + mk);
    }

    public void toRu() {
        // do something
        double rupi = getHrk() * 8.85;
        System.out.println("\nRus Rubl: " + rupi);
    }
}

// Main.java
public class Main {
    public static void main(String[] args) {
        HrvatskaValuta novcanik = new HrvatskaValuta(69);

        System.out.println(novcanik.getHrk() + " HRK\n");

        novcanik.toEuros();
        novcanik.toUSD();
        novcanik.toMk();
        novcanik.toRu();
    }
}

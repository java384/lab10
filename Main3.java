// Human.java
public class Human {
    String ime;
    String prezime;

    Human() {
        ime = "Ime";
        prezime = "Prezime";
    }

    Human(String ime, String prezime) {
        this.ime = ime;
        this.prezime = prezime;
    }

    void kretanje() {
        System.out.println("naš " + ime + " " + prezime + " hoda");
    }
}

// SuperHuman.java
public class SuperHuman extends Human {
    public SuperHuman() {
        ime = "SuperIme";
        prezime = "SuperPrezime";
    }

    public SuperHuman(String ime, String prezime) {
        this.ime = ime;
        this.prezime = prezime;
    }

    public void kretanje() {
        System.out.println(ime + " " + prezime + " leti do svemira!\n");
    }
}

// Main.java
public class Main {
    public static void main(String[] args) {
        SuperHuman Ante = new SuperHuman("Ante", "Etna");

        Ante.kretanje();
    }
}
